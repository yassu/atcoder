from heapq import heappop, heappush
from pprint import pprint
from sys import setrecursionlimit, stdin
from typing import Dict, Iterable, Set

INF: int = 2**62

setrecursionlimit(10**6)


def inputs(type_=int):
    ins = input().split(' ')
    ins = [x for x in ins if x != '']

    if isinstance(type_, Iterable):
        return [t(x) for t, x in zip(type_, ins)]
    else:
        return list(map(type_, ins))


def input_(type_=int):
    a, = inputs(type_)
    return a


inputi = input_


def inputstr():
    return input_(str)


# b/aの切り上げ
def ceil(b, a):
    return (a + b - 1) // a


class Heap:
    def __init__(self, data=None):
        if data is None:
            self.data = []
        else:
            self.data = data

    def push(self, x):
        heappush(self.data, x)

    def pop(self):
        return heappop(self.data)

    @property
    def min(self):
        return self.data[0]

    def __iter__(self):
        return iter(self.data)


def answer(res) -> None:
    print(res)
    exit()


# start coding
