atcoder
================================================================================

このプロジェクトは 個人のatcoderのための環境開発ツールです.

[atcoder-tools](https://github.com/kyuridenamida/atcoder-tools)のラッパーです.

# 機能

* 仮想環境の作成(poetry)
* タスク(invoke)
* flake8
* ipython
* isort
* mypy
* pudb
* yapf
* atcoder-tools

# 使い方

このディレクトリの直下にソースファイルを格納する`src/`ディレクトリを作成し, その下にソースコードファイルを置きます.

``` console
$ tree
.
├── LICENSE
├── README.markdown
├── poetry.lock
├── pyproject.toml
├── setup.cfg
├── src
│   ├── abc146
│   │   ├── A
│   │   │   ├── in_1.txt
│   │   │   ├── in_2.txt
│   │   │   ├── main.py
│   │   │   ├── metadata.json
│   │   │   ├── out_1.txt
│   │   │   └── out_2.txt
│   │   ├── B
│   │   │   ├── in_1.txt
│   │   │   ├── in_2.txt
...
│           └── out_3.txt
├── tasks.py
└── templates
    └── template.py

```

## 自分のためのメモ

以下のようにして srcのディレクトリをcloneする:

``` console
$ git clone https://gitlab.com/yassu/atcoder-src src  
```

# 提供するタスク

タスクは[pyinvoke](http://www.pyinvoke.org/)で管理されている.

以下のタスクが定義されている:

## create

以下の形式でコマンドを実行することで, コンテスト用のディレクトリを作成することができる

``` console
$ inv create --contest-name {contest-name} --lang {lang}
$ inv c -c {contest-name} -l {lang}    # alias
```

ここで, `{contest_name}`は`abc184`などのコンテストの名前であり, `{lang}`はプログラミング言語の名前です.

プログラミング言語は`cpp, java, rust, python, vim, d, cs`をサポートしています.

Ex.

``` console
inv c -c abc184 -l python
```

なお

* python => py
* c++ => cpp

のエイリアスを提供します.

## test

atcoderのテストを実行する

``` console
$ inv test --contest-name {contest-name} --lv {lv} --num {num}
$ inv t -c {contest-name} -l {lv} -n {num}
```

* `--contest-name`, `--lv`が指定されていない場合は, ディレクトリから計算される.
* `--num`が指定されていない場合は, 全てのテストを実行する.

## submit

atcoderに提出する.

atcoder-toolsでloginをしていない場合はログインしてから実行する.

``` console
$ inv submit --contest-name {contest-name} --lv {lv}
$ inv s -c {contest-name} -l {lv}
```

* `--contest-name`, `--lv`が指定されていない場合は, ディレクトリから計算される.

## run

atcoderのスクリプトを実行する.

現在は `python`のみに対応している.

``` console
$ inv run --contest-name {contest-name} --lv {lv} --num {num}
$ inv r -c {contest-name} -l {lv} -n {num}
```

* `--contest-name`, `--lv`が指定されていない場合は, ディレクトリから計算される.
* `--num`が指定されていない場合は, テストケースに対しては 実行しない
