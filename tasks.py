import re
from pathlib import Path
from string import ascii_uppercase
from sys import stderr
from time import sleep as _time_sleep
from typing import Dict, Optional, Tuple

from invoke import task
from invoke.exceptions import UnexpectedExit
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

LANG_ALIASES: Dict[str, str] = {
    'py': 'python',
    'c++': 'cpp',
}


def get_ext(lang: str) -> str:
    return {'python': '.py', 'cpp': '.cpp'}[lang]


def get_here() -> Path:
    return Path(__file__).parent.absolute()


def get_src_dir() -> Path:
    return get_here() / 'src'


@task(aliases=['c'])
def create(context, contest_name, lang):
    """
    lang is in [cpp, java, rust, python, vim, d, cs].
    """
    lang = LANG_ALIASES.get(lang, lang)
    template = get_here() / ('templates/template' + get_ext(lang))
    src_path = get_here() / 'src/'
    if template.exists():
        template_option = f'--template {template}'
    else:
        template_option = ''

    contest_dir = src_path / contest_name
    if (contest_dir).exists():
        stderr.write(f'{contest_dir} is exists.\n')
    else:
        context.run(
            f'atcoder-tools gen {contest_name} '
            f'--lang {lang} --workspace {src_path} {template_option}',
            echo=True)


def get_problem_info(contest_name: Optional[str],
                     lv: Optional[str]) -> Tuple[Optional[str], Optional[str]]:
    if lv is not None:
        lv = lv.upper()

    if contest_name is not None and lv is not None:
        return (contest_name, lv)

    src_dir = get_src_dir()
    rels = str(Path.cwd().relative_to(src_dir)).split('/')

    if len(rels) == 1:
        contest_name, = rels
    elif len(rels) == 2:
        if rels[1] not in ascii_uppercase:
            raise ValueError(f'{rels[1]} is not a lv.')

        contest_name, lv = rels

    if contest_name is None:
        raise ValueError('contest_name is not declared.')
    if lv is None:
        raise ValueError('lv is not declared.')

    return (contest_name, lv)


@task(aliases=['t'])
def test(context, contest_name=None, lv=None, num=None):
    """ contest_name and lv is computed by path and arguments """
    contest_name, lv = get_problem_info(contest_name, lv)

    if (contest_name, lv) == (None, None):
        print('Problem is not declared.')
        return

    dir_ = get_here() / f'src/{contest_name}/{lv}'
    cmd = f'cd {dir_}'
    cmd = cmd + ' && atcoder-tools test'
    if num:
        cmd += f' --num {num}'

    context.run(cmd, echo=True)


@task(aliases=['s'])
def submit(context, contest_name=None, lv=None, force=False):
    contest_name, lv = get_problem_info(contest_name, lv)

    if (contest_name, lv) == (None, None):
        print('Problem is not declared.')
        return

    dir_ = get_here() / f'src/{contest_name}/{lv}'
    cmd = f'atcoder-tools submit --dir {dir_} --unlock-safety'
    if force:
        cmd += ' --force'

    context.run(cmd, echo=True)


@task(aliases=['r'])
def run(context, contest_name=None, lv=None, num=None):
    contest_name, lv = get_problem_info(contest_name, lv)

    if (contest_name, lv) == (None, None):
        print('Problem is not declared.')
        return

    dir_ = get_here() / f'src/{contest_name}/{lv}'
    main_file = dir_ / 'main.py'
    cmd = f'time python {main_file}'

    if num is not None:
        test_filename = dir_ / f'in_{num}.txt'
        cmd = f'cat {test_filename} | {cmd}'

    context.run(cmd, echo=True)

    cmd2 = cmd.replace('python', 'pypy3.7')
    context.run(cmd2, echo=True)


class MainFileChangeHandler(FileSystemEventHandler):

    def __init__(self, context):
        super().__init__()
        self.context = context

    def on_modified(self, event):
        src_dir = get_src_dir()
        path = Path(event.src_path).absolute()

        if not str(path).startswith(str(src_dir)):
            return

        def is_target(basename):
            if basename == 'main.py':
                return True
            elif basename.startswith('in_') and basename.endswith('.txt'):
                return True
            elif basename.startswith('out_') and basename.endswith('.txt'):
                return True
            else:
                return False

        basename = str(path).split('/')[-1]
        if not is_target(basename):
            return

        rel = str(path.relative_to(src_dir))
        contest_name, lv, _ = rel.split('/')

        prob_dir = src_dir / f'{contest_name}/{lv}'
        cmd = f'cd {prob_dir}'
        cmd += ' && atcoder-tools test'

        try:
            self.context.run(cmd, echo=True)
        except UnexpectedExit:
            ...


@task(aliases=['watch', 'w'])
def watch_main(context):
    src_dir = get_src_dir()
    event_handler = MainFileChangeHandler(context)
    observer = Observer()
    observer.schedule(event_handler, str(src_dir), recursive=True)
    observer.start()

    try:
        while True:
            _time_sleep(0.1)
    except KeyboardInterrupt:
        observer.stop()


@task
def check_pdir(context, contest_name=None, lv=None):
    contest_name, lv = get_problem_info(contest_name, lv)
    print(106, contest_name, lv)
    if (contest_name, lv) == (None, None):
        print('Problem is not declared.')
        return

    print(get_here() / f'src/{contest_name}/{lv}')
